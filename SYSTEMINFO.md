# VMware vSphere

Vendor: VMware
Homepage: https://www.vmware.com/

Product: vSphere
Product Page: https://www.vmware.com/products/cloud-infrastructure/vsphere

## Introduction
We classify VMware vSphere into the Cloud domain as VMware vSphere is a comprehensive cloud computing virtualization platform for managing virtual machines and infrastructure.

## Why Integrate
The VMware vSphere adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware vSphere. With this adapter you have the ability to perform operations on items such as:

- Network
- Service
- Appliance
- Firewall

## Additional Product Documentation
The [API documents for VMware vSphere](https://vdc-repo.vmware.com/vmwb-repository/dcr-public/d1902b0e-d479-46bf-8ac9-cee0e31e8ec0/07ce8dbd-db48-4261-9b8f-c6d3ad8ba472/index.html)

