# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Vmware_vsphere System. The API that was used to build the adapter for Vmware_vsphere is usually available in the report directory of this adapter. The adapter utilizes the Vmware_vsphere API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The VMware vSphere adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware vSphere. With this adapter you have the ability to perform operations on items such as:

- Network
- Service
- Appliance
- Firewall

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
