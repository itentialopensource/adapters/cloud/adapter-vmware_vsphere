
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:42PM

See merge request itentialopensource/adapters/adapter-vmware_vsphere!21

---

## 0.4.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-vmware_vsphere!19

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:59PM

See merge request itentialopensource/adapters/adapter-vmware_vsphere!18

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:11PM

See merge request itentialopensource/adapters/adapter-vmware_vsphere!17

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!16

---

## 0.3.6 [03-28-2024]

* Changes made at 2024.03.28_13:21PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!15

---

## 0.3.5 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!14

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.13_13:17PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!13

---

## 0.3.3 [03-11-2024]

* Changes made at 2024.03.11_15:35PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!12

---

## 0.3.2 [02-28-2024]

* Changes made at 2024.02.28_11:49AM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!11

---

## 0.3.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!10

---

## 0.3.0 [12-18-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!9

---

## 0.2.0 [05-23-2022]

* Minor/adapt 2059:: Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!3

---

## 0.1.3 [08-02-2021]

- Task- listLibraries 
  - Change- Add /com/vmware
- Task- listLibraryItems
  - Change- Add /com/vmware
  - Change- library_id
- Task- getLibraryItemDetails
  - Change- Add /com/vmware
  - Change- Add id: before query

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!2

---

## 0.1.2 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-vmware_vsphere!1

---

## 0.1.1 [01-04-2021]

- Initial Commit

See commit f382a1f

---
