## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for VMware vSphere. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for VMware vSphere.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Vmware_vsphere. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getConsoleCLIAccess(callback)</td>
    <td style="padding:15px">get Console CLI Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/consolecli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uodateConsoleCLIAccess(body, callback)</td>
    <td style="padding:15px">uodate Console CLI Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/consolecli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDCUIAccess(callback)</td>
    <td style="padding:15px">get DCUI Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/dcui?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uodateDCUIAccess(body, callback)</td>
    <td style="padding:15px">uodate DCUI Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/dcui?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getShellAccess(callback)</td>
    <td style="padding:15px">get Shell Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/shell?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uodateShellAccess(body, callback)</td>
    <td style="padding:15px">uodate Shell Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/shell?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSHAccess(callback)</td>
    <td style="padding:15px">get SSH Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSSHAccess(body, callback)</td>
    <td style="padding:15px">update SSH Access</td>
    <td style="padding:15px">{base_path}/{version}/appliance/access/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceServicesHealth(callback)</td>
    <td style="padding:15px">get Appliance services health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/applmgmt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatabaseStorageHealth(callback)</td>
    <td style="padding:15px">get Database storage health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/database-storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoadHealth(callback)</td>
    <td style="padding:15px">get Load Health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMemoryHealth(callback)</td>
    <td style="padding:15px">get Memory Health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/mem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwarePackagesHealth(callback)</td>
    <td style="padding:15px">get Software packages health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/software-packages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStorageHealth(callback)</td>
    <td style="padding:15px">get Storage health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwapHealth(callback)</td>
    <td style="padding:15px">get Swap health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/swap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemHealth(callback)</td>
    <td style="padding:15px">get System health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemLastcheckHealth(callback)</td>
    <td style="padding:15px">get System lastcheck health</td>
    <td style="padding:15px">{base_path}/{version}/appliance/health/system/lastcheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMonitoring(callback)</td>
    <td style="padding:15px">List Monitoring</td>
    <td style="padding:15px">{base_path}/{version}/appliance/monitoring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringDetails(item, callback)</td>
    <td style="padding:15px">get Monitoring Details</td>
    <td style="padding:15px">{base_path}/{version}/appliance/monitoring/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryMonitoringStats(itemInterval, itemEndTime, itemNames1, itemNames2, itemStartTime, itemFunction, callback)</td>
    <td style="padding:15px">Query Monitoring stats</td>
    <td style="padding:15px">{base_path}/{version}/appliance/monitoring/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSNMPConfiguration(callback)</td>
    <td style="padding:15px">Return an SNMP agent configuration.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSNMPConfiguration(body, callback)</td>
    <td style="padding:15px">Set SNMP configuration</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSNMP(callback)</td>
    <td style="padding:15px">Start a disabled SNMP agent.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSNMP(callback)</td>
    <td style="padding:15px">Stop an enabled SNMP agent.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSNMPStats(callback)</td>
    <td style="padding:15px">Generate diagnostics report for snmp agent.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testSNMP(callback)</td>
    <td style="padding:15px">Send a warmStart notification to all configured traps and inform destinations (see RFC 3418).</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateSNMPHash(body, callback)</td>
    <td style="padding:15px">Generate localized keys for secure SNMPv3 communications.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp/hash?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetSNMPFactorySettings(callback)</td>
    <td style="padding:15px">Restore settings to factory defaults.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSNMPLimits(callback)</td>
    <td style="padding:15px">Get SNMP limits</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/monitoring/snmp/limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSSearchDomains(callback)</td>
    <td style="padding:15px">DNS search domains</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDNSSEARCHDomain(body, callback)</td>
    <td style="padding:15px">Set DNS Search Domains</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDNSSearchDomain(body, callback)</td>
    <td style="padding:15px">Add DNS Search Domain</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainName(callback)</td>
    <td style="padding:15px">Get the Fully Qualified Doman Name.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/hostname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDomainName(body, callback)</td>
    <td style="padding:15px">Set the Fully Qualified Domain Name.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/hostname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testDomainName(body, callback)</td>
    <td style="padding:15px">Test the Fully Qualified Domain Name.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/hostname/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSservers(callback)</td>
    <td style="padding:15px">get DNS servers</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDNSserver(body, callback)</td>
    <td style="padding:15px">add DNS servers</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSserver(body, callback)</td>
    <td style="padding:15px">update DNS servers</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testDNSservers(body, callback)</td>
    <td style="padding:15px">Test if dns servers are reachable.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/dns/servers/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaces(callback)</td>
    <td style="padding:15px">get Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceDetails(interfaceParam, callback)</td>
    <td style="padding:15px">Get information about a particular network interface.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networking/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv4(callback)</td>
    <td style="padding:15px">get IPv4</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/ipv4?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setIPv4(body, callback)</td>
    <td style="padding:15px">set IPv4</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/ipv4?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv4Details(body, callback)</td>
    <td style="padding:15px">set IPv4</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/ipv4/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewIPv4(body, callback)</td>
    <td style="padding:15px">renew IPv4</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/ipv4/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv6(callback)</td>
    <td style="padding:15px">Get IPv6</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/ipv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setIPv6(body, callback)</td>
    <td style="padding:15px">set IPv6</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/ipv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv6Details(body, callback)</td>
    <td style="padding:15px">get IPv6 Details</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/ipv6/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNTPConfigurationStatus(callback)</td>
    <td style="padding:15px">Get the NTP configuration status</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testNTPConfiguration(body, callback)</td>
    <td style="padding:15px">test NTP configuration</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/ntp/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNTPServer(body, callback)</td>
    <td style="padding:15px">add NTP server</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/ntp/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setNTPServers(body, callback)</td>
    <td style="padding:15px">set NTP servers</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/ntp/server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNTPServer(body, callback)</td>
    <td style="padding:15px">delete NTP server</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/ntp/server/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProxyConfigurations(callback)</td>
    <td style="padding:15px">Get proxy configurations</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setProxyConfiguration(body, callback)</td>
    <td style="padding:15px">set Proxy Configuration</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testProxyConfiguration(body, callback)</td>
    <td style="padding:15px">test Proxy Configuration</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/proxy/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProxyConfiguration(body, callback)</td>
    <td style="padding:15px">Delete Proxy Configuration</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/proxy/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkRoutes(callback)</td>
    <td style="padding:15px">List Network Routes</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setNetworkRoutes(body, callback)</td>
    <td style="padding:15px">Set Network Routes</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testNetworkRoutes(body, callback)</td>
    <td style="padding:15px">test Network Routes</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/routes/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkRoutes(body, callback)</td>
    <td style="padding:15px">Delete Network Routes</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/routes/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkRoute(body, callback)</td>
    <td style="padding:15px">add Network Routes</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupJob(callback)</td>
    <td style="padding:15px">get Backup job</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBackupJob(body, callback)</td>
    <td style="padding:15px">create Backup job</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateBackupjJob(body, callback)</td>
    <td style="padding:15px">validate Backup job</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelBackupJob(jobid, callback)</td>
    <td style="padding:15px">cancel Backup job</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/job/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupJobStatus(jobid, callback)</td>
    <td style="padding:15px">get Backup job status</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupJobDetails(callback)</td>
    <td style="padding:15px">Get Backup job details</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/job/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupParts(callback)</td>
    <td style="padding:15px">Get a list of the backup parts</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/parts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupPartSize(partsid, callback)</td>
    <td style="padding:15px">Get size of the optional part</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/parts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupSchedule(callback)</td>
    <td style="padding:15px">get Backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBackupSchedule(scheduleid, body, callback)</td>
    <td style="padding:15px">create Backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupScheduleDetails(scheduleid, callback)</td>
    <td style="padding:15px">get Backup schedule Details</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBackupSchedule(scheduleid, callback)</td>
    <td style="padding:15px">delete Backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBackupSchedule(scheduleid, body, callback)</td>
    <td style="padding:15px">update Backup schedule</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/backup/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestoreJob(callback)</td>
    <td style="padding:15px">get Restore job</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/restore/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRestoreJob(body, callback)</td>
    <td style="padding:15px">create Restore job</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/restore/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelRestoreJob(callback)</td>
    <td style="padding:15px">cancel Restore job</td>
    <td style="padding:15px">{base_path}/{version}/appliance/recovery/restore/job/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemStorage(callback)</td>
    <td style="padding:15px">get system Storage</td>
    <td style="padding:15px">{base_path}/{version}/appliance/system/storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resizeSystemStorage(callback)</td>
    <td style="padding:15px">Resize all partitions to 100 percent of disk size</td>
    <td style="padding:15px">{base_path}/{version}/appliance/system/storage/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemTime(callback)</td>
    <td style="padding:15px">get System Time</td>
    <td style="padding:15px">{base_path}/{version}/appliance/system/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemUptime(callback)</td>
    <td style="padding:15px">get System Uptime</td>
    <td style="padding:15px">{base_path}/{version}/appliance/system/uptime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemVersion(callback)</td>
    <td style="padding:15px">get System Version</td>
    <td style="padding:15px">{base_path}/{version}/appliance/system/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemShutdownOperations(callback)</td>
    <td style="padding:15px">Get System Shutdown operations</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/shutdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startSystemShutdown(body, callback)</td>
    <td style="padding:15px">start System Shutdown</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/shutdown/poweroff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelSystemShutdown(callback)</td>
    <td style="padding:15px">Cancel System Shutdown</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/shutdown/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartSystem(body, callback)</td>
    <td style="padding:15px">Restart System</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/shutdown/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemUpdateRepository(callback)</td>
    <td style="padding:15px">Get update repository configuration.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/system/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSystemUpdateRepository(body, callback)</td>
    <td style="padding:15px">Set update repository configuration.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/system/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimesync(callback)</td>
    <td style="padding:15px">Get Timesync</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/timesync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTimesync(body, callback)</td>
    <td style="padding:15px">set Timesync</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/timesync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkvCenterServerUpdate(sourceType, callback)</td>
    <td style="padding:15px">Check vCenter Server Update - URL & CD-ROM</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update/pending?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvCenterServerUpdateVersionDetails(version, callback)</td>
    <td style="padding:15px">Get vCenter Server Update - Version Details</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update/pending/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createvCenterServerUpdate(action, version, callback)</td>
    <td style="padding:15px">create vCenter Server Update</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update/pending/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvCenterServerUpdatePolicy(callback)</td>
    <td style="padding:15px">get vCenter Server Update Policy</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatevCenterServerUpdatePolicy(callback)</td>
    <td style="padding:15px">update vCenter Server Update Policy</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvCenterServerUpdateStagedUpdate(callback)</td>
    <td style="padding:15px">get vCenter Server Staged Update</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update/staged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletevCenterServerUpdateStagedUpdate(callback)</td>
    <td style="padding:15px">delete vCenter Server Staged Update</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update/staged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvCenterServerUpdateStatus(callback)</td>
    <td style="padding:15px">get vCenter Server Update Status</td>
    <td style="padding:15px">{base_path}/{version}/appliance/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInboundFirewallRules(callback)</td>
    <td style="padding:15px">Get ordered list of inbound IP addresses that are allowed or denied by firewall.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/firewall/addr/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInboundFirewallRule(body, callback)</td>
    <td style="padding:15px">Create inbound firewall rule</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/firewall/addr/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceInboundFirewallRule(body, callback)</td>
    <td style="padding:15px">Replace all inbound firewall rules</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/firewall/addr/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInboundFirewallRule(body, callback)</td>
    <td style="padding:15px">Delete inbound firewall rules</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/networking/firewall/addr/inbound/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(callback)</td>
    <td style="padding:15px">Get Services</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDetails(body, callback)</td>
    <td style="padding:15px">get specific service details</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/services/status/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">controlServices(body, callback)</td>
    <td style="padding:15px">Control Services</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/services/control?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartServices(body, callback)</td>
    <td style="padding:15px">Restart Services</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/services/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopServices(body, callback)</td>
    <td style="padding:15px">stop Services</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/services/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalAccounts(callback)</td>
    <td style="padding:15px">Get Local accounts</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/local-accounts/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLocalaccounts(body, callback)</td>
    <td style="padding:15px">Create Local accounts</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/local-accounts/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLocalAccounts(body, callback)</td>
    <td style="padding:15px">update Local accounts</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/local-accounts/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalAccountsDetails(localUser, callback)</td>
    <td style="padding:15px">Get Local accounts Details</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/local-accounts/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeLocalAccounts(callback)</td>
    <td style="padding:15px">Remove Local accounts</td>
    <td style="padding:15px">{base_path}/{version}/appliance/techpreview/local-accounts/user/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVmonServices(callback)</td>
    <td style="padding:15px">Get Vmon Services</td>
    <td style="padding:15px">{base_path}/{version}/appliance/vmon/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVmonServiceDetails(service, callback)</td>
    <td style="padding:15px">Get details about specific vmon service</td>
    <td style="padding:15px">{base_path}/{version}/appliance/vmon/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVmonservice(service, body, callback)</td>
    <td style="padding:15px">Update specific Vmon service</td>
    <td style="padding:15px">{base_path}/{version}/appliance/vmon/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartVmonService(service, callback)</td>
    <td style="padding:15px">Restart specific Vmon service</td>
    <td style="padding:15px">{base_path}/{version}/appliance/vmon/service/{pathv1}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startVmonservice(service, callback)</td>
    <td style="padding:15px">Start Specific Vmon service</td>
    <td style="padding:15px">{base_path}/{version}/appliance/vmon/service/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopVmonService(service, callback)</td>
    <td style="padding:15px">Stop specific Vmon service</td>
    <td style="padding:15px">{base_path}/{version}/appliance/vmon/service/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLibraries(callback)</td>
    <td style="padding:15px">List Content Libraries</td>
    <td style="padding:15px">{base_path}/{version}/content/library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLibraryDetails(library, callback)</td>
    <td style="padding:15px">Get details of single library</td>
    <td style="padding:15px">{base_path}/{version}/content/library/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLibraryItems(libraryId, callback)</td>
    <td style="padding:15px">List all items in a specific library</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/content/library/item?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLibraryItemDetails(item, callback)</td>
    <td style="padding:15px">List details of a library item</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/content/library/item/id:{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFilesInlibraryItem(libraryItemId, callback)</td>
    <td style="padding:15px">List files in a specific library item</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/content/library/item/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageInLibraryItem(libraryItemId, callback)</td>
    <td style="padding:15px">List storage items in a specific library item</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/content/library/item/storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">takeActionOnLibraryItem(item, action, body, callback)</td>
    <td style="padding:15px">Take action on a Library Item</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/ovf/library-item/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLocalLibrary(body, callback)</td>
    <td style="padding:15px">Create a Local Library</td>
    <td style="padding:15px">{base_path}/{version}/content/local-library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLocalLibraries(callback)</td>
    <td style="padding:15px">List all local libraries</td>
    <td style="padding:15px">{base_path}/{version}/content/local-library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalLibraryDetails(library, callback)</td>
    <td style="padding:15px">Get details of a local library</td>
    <td style="padding:15px">{base_path}/{version}/content/local-library/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLocalLibrary(id, callback)</td>
    <td style="padding:15px">Delete a local library</td>
    <td style="padding:15px">{base_path}/{version}/content/local-library/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubscribedLibrary(body, callback)</td>
    <td style="padding:15px">Create Subscribed Library</td>
    <td style="padding:15px">{base_path}/{version}/content/subscribed-library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubscribedlibraryies(callback)</td>
    <td style="padding:15px">List all subscribed libraries</td>
    <td style="padding:15px">{base_path}/{version}/content/subscribed-library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubscribedLibraryDetails(library, callback)</td>
    <td style="padding:15px">Get details of a subscribed library</td>
    <td style="padding:15px">{base_path}/{version}/content/subscribed-library/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscribedLibrary(id, callback)</td>
    <td style="padding:15px">Delete a subscribed library</td>
    <td style="padding:15px">{base_path}/{version}/content/subscribed-library/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">evictCachedContentSubscribedLibrary(id, action, callback)</td>
    <td style="padding:15px">Evict cached content of on-demand subscribed library</td>
    <td style="padding:15px">{base_path}/{version}/content/subscribed-library/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
